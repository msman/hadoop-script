#!/bin/bash

#Author: Nicolas Thouvenin <thouvenin.n@gmail.com>

#Formatage nécessaire de hdfs
format=0

#Mode local/pseudo
OPT_M="local"

#Operation réaliser (start/stop)
operation=""

#path cfg file
HADOOP_ETC=$HADOOP_PREFIX/etc/hadoop

function start {
    ${HADOOP_PREFIX}/sbin/hadoop-daemon.sh start namenode
    ${HADOOP_PREFIX}/sbin/hadoop-daemon.sh start datanode
    ${HADOOP_PREFIX}/sbin/hadoop-daemon.sh start secondarynamenode
    ${HADOOP_PREFIX}/sbin/yarn-daemon.sh start resourcemanager
    ${HADOOP_PREFIX}/sbin/yarn-daemon.sh start nodemanager
    ${HADOOP_PREFIX}/sbin/mr-jobhistory-daemon.sh start historyserver
}

function stop {
    ${HADOOP_PREFIX}/sbin/mr-jobhistory-daemon.sh stop historyserver
    ${HADOOP_PREFIX}/sbin/yarn-daemon.sh stop nodemanager
    ${HADOOP_PREFIX}/sbin/yarn-daemon.sh stop resourcemanager
    ${HADOOP_PREFIX}/sbin/hadoop-daemon.sh stop secondarynamenode
    ${HADOOP_PREFIX}/sbin/hadoop-daemon.sh stop datanode
    ${HADOOP_PREFIX}/sbin/hadoop-daemon.sh stop namenode
}

function checkHDFSDir {
    if [[ ! -e "$HADOOP_PREFIX/hdfs" ]]; then
        echo "hdfs directory doesn't exist -> created"
        mkdir ${HADOOP_PREFIX}/hdfs
        mkdir ${HADOOP_PREFIX}/hdfs/namenode
        mkdir ${HADOOP_PREFIX}/hdfs/datanode
        format=1
    else
        if [[ ! -e "$HADOOP_PREFIX/hdfs/namenode" ]]; then
            echo "hdfs/namenode directory doesn't exist -> created"
            mkdir ${HADOOP_PREFIX}/hdfs/namenode
            format=1
        fi
        if [[ ! -e "$HADOOP_PREFIX/hdfs/datanode" ]]; then
            echo "hdfs/datanode directory doesn't exist -> created"
            mkdir ${HADOOP_PREFIX}/hdfs/datanode
            format=1
        fi
    fi
}

function  checkFormat {
    if [ $format -eq 1 ]; then
        cd ${HADOOP_PREFIX}/hdfs
        hdfs namenode -format
    fi
}

function printInfosInterfaces {
    echo ""
    jps
    echo ""
    echo "http://localhost:50070/"
    echo "http://localhost:8088/"
}

function computeInput {
    while getopts :m: FLAG; do
        case $FLAG in
            m)
            OPT_M=$OPTARG
            if [ "$OPT_M" != "local" ]; then
                if [ "$OPT_M" != "pseudo" ]; then
                    echo "Mode should be local/pseudo --> default:local"
                    OPT_M="local"
                fi
            fi
            ;;
            \?)
            echo -e \\n"Option -$OPTARG not allowed."
            ;;
        esac
    done
}

function checkHadoopPrefix {
    if test -z $HADOOP_PREFIX
        then
        echo "Variable \$HADOOP_PREFIX must be set"
        exit 0
    fi
}

function checkBin {
    which hadoop-daemon.sh 1>/dev/null
    if [ $? -ne 0 ]
        then
        echo "Variable $\HADOOP_PREFIX/sbin must be in \$PATH"
        exit 0
    fi
}

function checkJavaHome {
    if test -z $JAVA_HOME
        then
        echo "Variable \$JAVA_HOME must be set"
        exit 0
    fi
}

function checkHadoopClasspath {
    if test -z $HADOOP_CLASSPATH
        then
        echo "Variable \$HADOOP_CLASSPATH must be set"
        exit 0
    fi
}

function checkEnv {
    checkHadoopPrefix
    checkBin
    checkJavaHome
    checkHadoopClasspath
}

function checkCfgFileLocal {
    if [[ ! -e "${HADOOP_ETC}/core-site.xml-local" ]]; then
        echo "CFG File Missing : core-site.xml-local"
        exit 1
    fi
    if [[ ! -e "$HADOOP_ETC/hdfs-site.xml-local" ]]; then
        echo "CFG File Missing : hdfs-site.xml-local"
        exit 1
    fi
    if [[ ! -e "$HADOOP_ETC/mapred-site.xml-local" ]]; then
        echo "CFG File Missing : mapred-site.xml-local"
        exit 1
    fi
    if [[ ! -e "$HADOOP_ETC/yarn-site.xml-local" ]]; then
        echo "CFG File Missing : yarn-site.xml-local"
        exit 1
    fi
}

function checkCfgFilePseudo {
    if [[ ! -e "$HADOOP_ETC/core-site.xml-pseudoD" ]]; then
        echo "CFG File Missing : core-site.xml-pseudoD"
        exit 1
    fi
    if [[ ! -e "$HADOOP_ETC/hdfs-site.xml-pseudoD" ]]; then
        echo "CFG File Missing : hdfs-site.xml-pseudoD"
        exit 1
    fi
    if [[ ! -e "$HADOOP_ETC/mapred-site.xml-pseudoD" ]]; then
        echo "CFG File Missing : mapred-site.xml-pseudoD"
        exit 1
    fi
    if [[ ! -e "$HADOOP_ETC/yarn-site.xml-pseudoD" ]]; then
        echo "CFG File Missing : yarn-site.xml-pseudoD"
        exit 1
    fi
}

function checkCfgFile {
    checkCfgFileLocal
    checkCfgFilePseudo
}

function checkVariables {
    if [[ $# -eq 3 ]]; then
        operation=$3
    else
        if [[ $# -eq 1 ]]; then
            operation=$1
        else
            exit 0
        fi
    fi
}

function setLocalMode {
    cp $HADOOP_ETC/core-site.xml-local $HADOOP_ETC/core-site.xml
    cp $HADOOP_ETC/hdfs-site.xml-local $HADOOP_ETC/hdfs-site.xml
    cp $HADOOP_ETC/mapred-site.xml-local $HADOOP_ETC/mapred-site.xml
    cp $HADOOP_ETC/yarn-site.xml-local $HADOOP_ETC/yarn-site.xml
}

function setPseudoMode {
    cp $HADOOP_ETC/core-site.xml-pseudoD $HADOOP_ETC/core-site.xml
    cp $HADOOP_ETC/hdfs-site.xml-pseudoD $HADOOP_ETC/hdfs-site.xml
    cp $HADOOP_ETC/mapred-site.xml-pseudoD $HADOOP_ETC/mapred-site.xml
    cp $HADOOP_ETC/yarn-site.xml-pseudoD $HADOOP_ETC/yarn-site.xml
}

function changeMode {
    if [[ $OPT_M == "local" ]]; then
        setLocalMode
    else
        setPseudoMode
    fi
}

checkEnv
checkCfgFile
computeInput $*
changeMode
checkVariables $*

if [[ $operation == "start" ]]; then
    checkHDFSDir
    checkFormat
    start
    printInfosInterfaces
fi

if [ $operation == "stop" ]; then
    stop
    echo ""
    jps
fi
exit 0
